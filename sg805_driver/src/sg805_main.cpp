#include "SG805RobotRos.h"


int main(int argc, char *argv[])
{
    //节点命名
    ros::init(argc, argv, "sg805_node");

    //私有参数获取句柄
    ros::NodeHandle nh("~");

    //IP和端口
    string sg805_ip;
    int sg805_port;

    //参数获取
    nh.param<string>("sg805_ip", sg805_ip, "127.0.0.1");
    nh.param<int>("sg805_port", sg805_port, 8080);

    //准备连接
    sg805RobotPtr->setServerIP(sg805_ip);
    sg805RobotPtr->setServerPort(sg805_port);


    //ros机械臂实例
    SG805RobotRos * sg805RobotRos = new SG805RobotRos();

    return 0;
}