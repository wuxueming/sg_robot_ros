#include "SG805Robot.h"

//构造函数
SG805Robot::SG805Robot()
{
}

//构造函数
SG805Robot::SG805Robot(string serverIP_, int serverPort_) : serverIP(serverIP_), serverPort(serverPort_)
{
    startConstruction();
}

SG805Robot::~SG805Robot()
{
}

//17位，范围为0~0x1ffff
//extern int yodaEncoderRange;

void SG805Robot::InitJointParam()
{
    //重要参数
    //旋转+180°(+3.1415926)，需要的节拍

    plu2angel[0] = 1024 * 18;          //2048
    plu2angel[1] = 1024 * 101;         //2048
    plu2angel[2] = 1024 * 101;         //2048
    plu2angel[3] = -1024 * 51;         //2048
    plu2angel[4] = -1024 * 50;         //2048
    plu2angel[5] = -1024 * 51;         //2048 * 51

    //零点参数 零点时，编码器的数据
    zeroPlu[0] = -1024;
    zeroPlu[1] = -1024;
    zeroPlu[2] = -1024;
    zeroPlu[3] = -1024;
    zeroPlu[4] = -1024;
    zeroPlu[5] = -1024;

    //编码器安装方向
    encoderPositiveOrNegative[0] = -1;
    encoderPositiveOrNegative[1] = -1;
    encoderPositiveOrNegative[2] = -1;
    encoderPositiveOrNegative[3] = -1;
    encoderPositiveOrNegative[4] = -1;
    encoderPositiveOrNegative[5] = -1;
    //32绿色  31红色
}

//设置IP
void SG805Robot::setServerIP(string s)
{
    serverIP = s;
}

//设置端口号
void SG805Robot::setServerPort(int port)
{
    serverPort = port;
}

//开始连接
void SG805Robot::startConstruction()
{
    InitJointParam();

#ifndef USE_ROS
    signal(SIGINT, MyFunctions::stop);
#endif
    std::cout << "客户端启动，尝试连接服务端  " << serverIP << ":" << serverPort << std::endl;
    // socket
    client_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (client_fd == -1)
    {
        std::cout << "Error: socket" << std::endl;
        exit(0);
    }

    //服务端 ip及程序端口号
    serverAddr.sin_family = AF_INET; //tcp IPv4
    serverAddr.sin_port = htons(serverPort);
    serverAddr.sin_addr.s_addr = inet_addr(serverIP.c_str());

    //尝试连接服务器
    isConnected = false;
    if (connect(client_fd, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
    {
        std::cout << "Error: connect" << std::endl;
        exit(0);
    }
    isConnected = true;

    //序号从1开始，绝对不可以从0开始，因为会让下位机误判认为是已经接受过的数据，那么下位机只会反馈数据，不会执行这个包的命令
    Sequence = 1;
    cout << "连接成功\n";

    //编码器数据清零
    memset(encoderAngle, 0, sizeof(encoderAngle));

    memset(&fast_point_handle, 0, sizeof(fast_point_handle));

    //rs485标志位，默认为关闭
    rs485_flag = false;

    //usart标志位，默认为关闭
    usart_flag = false;

    //根据末端具体情况设置IO




    //服务于 测距传感器
    timeoutPtr_distance = new std::chrono::milliseconds(1000);  //1000ms超时

    // 默认 上锁
    distanceSensorMutex.lock();

}

//断开网络
void SG805Robot::closeClient()
{
    cout << "断开连接" << endl;
    close(client_fd);
}

//启动服务器，并接收数据
void SG805Robot::listening()
{
    //cout << "listening  "<< MyFunctions::ok() <<"\n";
#ifdef USE_ROS
    while (ros::ok())
#else
    while (MyFunctions::ok())
#endif
    {
        //收数据
        recv_len = recv(client_fd, recvBuffer, sizeof(recvBuffer), 0);
#ifdef RECV_DISPLAY
        std::cout << "共接收: " << std::dec << recv_len << "b" << std::endl;
#endif
        //检测服务端是否断开连接
        if (strcmp((char *)recvBuffer, "exit") == 0)
        {
            std::cout << "服务器断开连接" << std::endl;
            break;
        }

        switch (recv_len)
        {
        case 2:

            //接收下位机确认信息
            std::cout << "共接收: " << std::dec << recv_len << "b" << std::endl;
            /*
             2023-06-22 
            if (!sendSuccess)
            { //如果已经成功，则没有再次进行校验的必要了，而且序列号等数据必定改动
                if (recvBuffer[0] == Sequence && recvBuffer[1] == send_check)
                {
                    sendSuccess = true;
                }
                else
                {
                    if (recvBuffer[0] != Sequence)
                    {
                        cout << "序列号不匹配" << endl;
                        cout << "recv[0] " << (int)recvBuffer[0] << "  ";
                        cout << "Sequence " << (int)Sequence << endl;
                    }
                    if (recvBuffer[1] != send_check)
                    {
                        std::cout << "共接收: " << std::dec << recv_len << "b" << std::endl;
                        cout << "check不匹配" << endl;
                    }
                }
            }

            if (sendSuccess)
            {
#ifdef SUCCESS_INFO
                cout << "校验结果 成功" << endl;
#endif
            }
            else
            {
                cout << "校验结果 失败" << endl;
            }
             2023-06-22 结束
            */

            if (!sendSuccess)
            {
                // 确认位和校验位同时出错
                if ((recvBuffer[0] == 0xcc)  && (recvBuffer[1] == send_check))
                {
                    sendSuccess = true;
                    std::cout << "数据传输正确" << std::endl;
                }
                
                else if ((recvBuffer[0] != 0xcc)  && (recvBuffer[1] != send_check))
                {
                    sendSuccess = false;
                    std::cout << "\033[31m第1类通讯错误,即将重传数据\033[0m" << std::endl;
                }

                // 确认位和校验位不同时出错
                else
                {
                    sendSuccess = true;
                    std::cout << "\033[31m第2类通讯错误,无须重传数据\033[0m" << std::endl;

                    if ( !((recvBuffer[0] == 0x33) || (recvBuffer[0] == 0xcc)) )
                    {
                        std::cout << "确认位数据 既不是正确编号,也不是错误编号 "
                            << (int)recvBuffer[0] << std::endl;
                    }

                    if(recvBuffer[1] != send_check)
                    {
                        std::cout << "校验位数据有误 " << std::endl;
                    }
                }
            }

            break;

        case LocationTCPLength:
            //接收下位机当前的信息
            if (CRC_Recv())
            { //校验位无误
                memcpy(&location, recvBuffer, LocationTCPDataLength);

                // for (int i = 0; i < recv_len; i++)
                // {
                //     std::cout << (int)recvBuffer[i] << " " ;
                // }
                // std::cout << "\n输出完毕\n\n";

                //在ros中，机械臂状态是无用的
#ifdef RECV_DISPLAY
                std::cout << "机械臂状态为 "; // << (int)location.state << std::endl;

                for (int i = 0; i < 7; i++)
                {
                    // std::cout<< "第" << i << "原始数据 ";
                    // for (int j = 0; j < 4; j ++)
                    // {
                    //     std::cout << (int)recvBuffer[ i*4 + j] << " ";
                    // }
                    //std::cout << (int)location.position[i] << std::endl;
                    std::cout << (int)location.position[i] << " ";
                }
                std::cout << std::endl
                          << std::endl
                          << std::endl
                          << std::endl;
#endif
            }
            else
            {
                std::cout << "下位机发送的数据有误，丢弃" << std::endl;
            }
            break;

        case USART_TO_TCP_SIZE:
            //接收到串口数据并打印
            usart_recv();

            if (usart_rx_len == 9)
            {
                //校验
                if (checkEncoderData(usartRXBuffer, usart_rx_len))
                {
                    int _index = getEncoderID(usartRXBuffer) - 1;

                    //获取编码器数据

                    encoderPosition[_index] = getEncoderPosition(usartRXBuffer);
                    encoderTurns[_index] = getEncoderTurns(encoderPosition[_index]);
                    encoderAngle[_index] = getEncoderAngle(encoderPosition[_index]);

                    std::cout << "编码器" << _index + 1 << " 圈数" << encoderTurns[_index]
                              << ",单圈位置" << encoderAngle[_index]
                              << ",位置为" << encoderPosition[_index] << std::endl;

#ifdef SUCCESS_INFO
                    std::cout << "编码器" << _index + 1 << "数值为" << encoderAngle[_index] << std::endl;
#endif
                }
                else
                {
                    std::cout << "编码器数据校验失败" << usart_rx_len << std::endl;
                }
            }
            else if (usart_rx_len == 7)
            {
                //校验
                if (checkEncoderData(usartRXBuffer, usart_rx_len))
                {
                    distanceData = getDistance(usartRXBuffer);
                    std::cout << "获得测距仪数据"  << distanceData <<std::endl;
                }
                else
                {
                    distanceData = -1;
                    std::cout << "测距仪数据有误" << std::endl;
                }

                // 无论数据是否有误,只要有响应,就应当判定,服务成功
                distanceSensorMutex.unlock();
            }
            

            break;

        default:
            //std::cout << "接收到了"<< recv_len << "字节" << std::endl;
            break;
        }

        usleep(1000);
    }
    cout << "主动断开连接" << endl;
    close(client_fd);
}

//心跳检测
void SG805Robot::keepAlive()
{
}

//传输轨迹数据
void SG805Robot::sendTrajectory()
{
    //发送长度数据
#ifdef SUCCESS_INFO
    cout << "send New   NumberOfPoints:" << NumberOfPoints << endl;
#endif
    send_len = 5;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = NEW;
    sendBuffer[2] = (NumberOfPoints >> 8) & 0xff;
    sendBuffer[3] = NumberOfPoints & 0xff;
    sendModul(); //调用发送模块发送数据
#ifdef SUCCESS_INFO
    cout << "send New over\n";
#endif

    //发送轨迹数据，一个数据点的大小为116byte，一个数据包最大1395(TCP最大1460)，最多一次发送12个数据点
    NumberOfFullPackages = NumberOfPoints / FullPointInTCP;
    NumberOfRestPoints = NumberOfPoints % FullPointInTCP;
#ifdef SUCCESS_INFO
    cout << "满包数量:" << (int)NumberOfFullPackages << ", 余包包含的点数:" << (int)NumberOfRestPoints << endl;
#endif
    writePointIndex = 0;
    sendBuffer[1] = PENDING;
    if (NumberOfFullPackages > 0)
    {
#ifdef SUCCESS_INFO
        cout << "发送满包轨迹数据" << endl;
#endif
        send_len = FullTCPLength; //1395
        for (int i = 0; i < NumberOfFullPackages; i++)
        {
            sendBuffer[0] = Sequence;

            // FullTCPDataLength = 1392
            memcpy(sendBuffer + 2, &trajectory[writePointIndex], FullTCPDataLength);

            //memcpy(&fake_trajectory[writePointIndex], sendBuffer + 2, FullTCPDataLength);

            writePointIndex += FullPointInTCP;

            sendModul(); //调用发送模块发送数据
        }
    }

    //余下的轨迹数据
    if (NumberOfRestPoints > 0)
    {
#ifdef SUCCESS_INFO
        cout << "发送余包轨迹数据" << endl;
#endif
        send_len = NumberOfRestPoints * PointSize + 3;
        sendBuffer[0] = Sequence;
        memcpy(sendBuffer + 2, &trajectory[writePointIndex], send_len - 3);
        writePointIndex += NumberOfRestPoints;
        sendModul(); //调用发送模块发送数据
    }

    //发送启动标志位
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = RUNING;
#ifdef SUCCESS_INFO
    cout << "RUNING\n";
#endif
    sendModul(); //调用发送模块发送数据
}

//对接收数据进行剩余校验，校验结果，0为有错误，1为没有错误
uint8_t SG805Robot::CRC_Recv()
{
    recv_check = 0;
    for (int i = 0; i < recv_len - 1; i++)
    {
        recv_check += recvBuffer[i];
    }
    recv_check &= 0xff;

    if (recvBuffer[recv_len - 1] != recv_check)
    {
        return 0;
    }
    return 1;
}

//对发送数据进行剩余校验
void SG805Robot::CRC_Send()
{
    send_check = 0;
    for (int i = 0; i < send_len - 1; i++)
    {
        send_check += sendBuffer[i];
    }
    send_check &= 0xff;
    sendBuffer[send_len - 1] = send_check;
}

//确保发送成功的模块
void SG805Robot::sendModul()
{
    CRC_Send();
    sendSuccess = false;
    int count = 0;

#ifdef USE_ROS
    while (!sendSuccess && ros::ok())
#else
    while (!sendSuccess && MyFunctions::ok())
#endif
    {
        send(client_fd, sendBuffer, send_len, 0);

        if (count != 0)
        {
            cout << "第" << count++ << "次发送数据\n";
        }

        //最多等待1s，否则重传
        for (int i = 0; i < 1000; i++)
        {
            if (sendSuccess)
            {
                break;
            }
            usleep(1000);
        }

        //最多重传5次，否则视为断开链接
        if (count > 5)
        {
            closeClient();
            ros::shutdown();
            exit(0);
        }
    }
    Sequence++;
    Sequence %= 0xff; //0~254之间
}

//判断是否已经停止运动
bool SG805Robot::isStopped()
{
    if (location.state == STOPPED)
    {
        return true;
    }
    return false;
}

//判断是否已经到达最终位姿
bool SG805Robot::isArrived()
{
    //如果处于停止
    for (int i = 0; i < 6; i++)
    {
        if (abs(location.position[i] - trajectory[NumberOfPoints - 1].position[i]))
        {
            return false;
        }
    }
    return true;
}

//打印轨迹信息
void SG805Robot::printTrajectory()
{
    printf("轨迹点数量:%d\n", NumberOfPoints);
    for (int i = 0; i < NumberOfPoints; i++)
    {
        // volatile int32_t duration;              //运行时间
        // volatile int16_t numberOfFullPeriod[8]; //0xffff周期的数量
        // volatile int16_t restPeriod[8];         //最后余下的周期，单位为1us，只有period >= 0xffff时，才有效
        // volatile int16_t numberOfPeriod[8];     //pwm周期数
        // volatile int32_t period[8];             //产生pwm的周期，单位为1us
        // volatile int32_t position[8];           //关节运行到的脉冲位置

        printf("第%d个点: duration:%d\n"
               "numberOfFullPeriod:%d,%d,%d,%d,%d,%d,%d,%d\n"
               "restPeriod:%d,%d,%d,%d,%d,%d,%d,%d\n"
               "numberOfPeriod:%d,%d,%d,%d,%d,%d,%d,%d\n"
               "period:%d,%d,%d,%d,%d,%d,%d,%d\n"
               "position:%d,%d,%d,%d,%d,%d,%d,%d\n\n",

               i,
               trajectory[i].duration,
               trajectory[i].numberOfFullPeriod[0],
               trajectory[i].numberOfFullPeriod[1],
               trajectory[i].numberOfFullPeriod[2],
               trajectory[i].numberOfFullPeriod[3],
               trajectory[i].numberOfFullPeriod[4],
               trajectory[i].numberOfFullPeriod[5],
               trajectory[i].numberOfFullPeriod[6],
               trajectory[i].numberOfFullPeriod[7],

               trajectory[i].restPeriod[0],
               trajectory[i].restPeriod[1],
               trajectory[i].restPeriod[2],
               trajectory[i].restPeriod[3],
               trajectory[i].restPeriod[4],
               trajectory[i].restPeriod[5],
               trajectory[i].restPeriod[6],
               trajectory[i].restPeriod[7],

               trajectory[i].numberOfPeriod[0],
               trajectory[i].numberOfPeriod[1],
               trajectory[i].numberOfPeriod[2],
               trajectory[i].numberOfPeriod[3],
               trajectory[i].numberOfPeriod[4],
               trajectory[i].numberOfPeriod[5],
               trajectory[i].numberOfPeriod[6],
               trajectory[i].numberOfPeriod[7],

               trajectory[i].period[0],
               trajectory[i].period[1],
               trajectory[i].period[2],
               trajectory[i].period[3],
               trajectory[i].period[4],
               trajectory[i].period[5],
               trajectory[i].period[6],
               trajectory[i].period[7],

               trajectory[i].position[0],
               trajectory[i].position[1],
               trajectory[i].position[2],
               trajectory[i].position[3],
               trajectory[i].position[4],
               trajectory[i].position[5],
               trajectory[i].position[6],
               trajectory[i].position[7]);
    }
}

//紧急制动
void SG805Robot::robot_stop()
{
    //发送启动标志位
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = STOPPED;
    cout << "STOPPED\n";
    sendModul(); //调用发送模块发送数据
}

//重新开启Location数据的上传，默认开启
void SG805Robot::upload_start()
{
    //发送启动标志位
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = UPLOAD_START;
#ifdef SUCCESS_INFO
    cout << "UPLOAD_START\n";
#endif
    sendModul(); //调用发送模块发送数据
}

//关闭Location数据的上传，默认开启
void SG805Robot::upload_stop()
{
    //发送关闭标志位
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = UPLOAD_STOP;
#ifdef SUCCESS_INFO
    cout << "UPLOAD_STOP\n";
#endif
    sendModul(); //调用发送模块发送数据
}

//打开PWM
void SG805Robot::pwm_start()
{
    //发送pwm
    send_len = 3 + sizeof(pwm_handle);
    sendBuffer[0] = Sequence;
    sendBuffer[1] = PWM_START;
    memcpy(sendBuffer + 2, &pwm_handle, sizeof(pwm_handle));
#ifdef SUCCESS_INFO
    cout << "PWM_START\n";
#endif
    sendModul(); //调用发送模块发送数据
}

//关闭PWM
void SG805Robot::pwm_stop()
{
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = PWM_STOP;
#ifdef SUCCESS_INFO
    cout << "PWM_STOP\n";
#endif
    sendModul(); //调用发送模块发送数据
}

//开启USART通信中断，并通过中断方式向上位机发送接收到的串口数据，需要包含波特率等信息，设置在usart_setting结构实例中
void SG805Robot::usart_start()
{
    // uint32_t BaudRate;
    // uint32_t WordLength;
    // uint32_t StopBits;
    // uint32_t Parity;
    // uint32_t Mode; //0:收发，1只收不发，2只发不收，其他数值 则默认为收发
    // uint32_t HwFlowCtl;
    // uint32_t OverSampling;
    //通常在此处配置即可
    uart_setting.BaudRate = 19200;
    uart_setting.WordLength = 0;
    uart_setting.StopBits = 0;
    uart_setting.Parity = 0;
    uart_setting.Mode = 0;
    uart_setting.HwFlowCtl = 0;
    uart_setting.OverSampling = 0;

    send_len = 3 + sizeof(uart_setting);
    sendBuffer[0] = Sequence;
    sendBuffer[1] = USART_START;
    memcpy(sendBuffer + 2, &uart_setting, sizeof(uart_setting));
#ifdef SUCCESS_INFO
    cout << "USART_START\n";
#endif
    sendModul();

    usart_flag = true;
}

//关闭USART通信中断
void SG805Robot::usart_stop()
{
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = USART_STOP;
#ifdef SUCCESS_INFO
    cout << "USART_STOP\n";
#endif
    sendModul();

    usart_flag = false;
}

//发送数据到串口
void SG805Robot::usart_send()
{
    //获得长度   串口数据 + 序号 + 功能 + 校验
    send_len = 3 + usart_tx_len;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = USART_SEND;
    //向下发送的数据可以不定长度，仅仅放入串口数据即可
    memcpy(sendBuffer + 2, usartTXBuffer, usart_tx_len);
#ifdef SUCCESS_INFO
    cout << "USART_SEND\n";
#endif
    sendModul();
}

//接收串口数据并上传至上位机
void SG805Robot::usart_recv()
{
    /**
     * 长度信息       数据       CRC
     *    0         1-255      256
     * 
     * 共258字节长度，数据段数据最多有255位有效，其余为0
    **/

    //从结构中获取
    usart_rx_len = (int)recvBuffer[0];

    //把数据保存在串口缓存中
    memcpy(usartRXBuffer, recvBuffer + 1, usart_rx_len);

    //#ifdef SUCCESS_INFO
    //打印
    cout << "接收到串口数据 长度为 : " << usart_rx_len << endl;
    for (int i = 0; i < usart_rx_len; i++)
    {
        cout << (char)usartRXBuffer[i];
    }
    cout << hex << endl;
    for (int i = 0; i < usart_rx_len; i++)
    {
        cout << (int)usartRXBuffer[i] << " ";
    }
    cout << dec << endl;
    //#endif
}

//PIN0高电平
void SG805Robot::pin0_on()
{
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = PIN0_ON;
    cout << "PIN0_ON\n";
    sendModul();
}

//PIN0低电平
void SG805Robot::pin0_off()
{
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = PIN0_OFF;
    cout << "PIN0_OFF\n";
    sendModul();
}

//PIN1高电平
void SG805Robot::pin1_on()
{
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = PIN1_ON;
    cout << "PIN1_ON\n";
    sendModul();
}

//PIN1低电平
void SG805Robot::pin1_off()
{
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = PIN1_OFF;
    cout << "PIN1_OFF\n";
    sendModul();
}

//ENABLE使能翻转
void SG805Robot::toggle_enable_pins()
{
    send_len = 5;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = TOGGLE_ENABLE_PINS;
    sendBuffer[2] = (enable_pins >> 8) & 0xff;
    sendBuffer[3] = enable_pins & 0xff;
    cout << "TOGGLE_ENABLE_PINS\n";
    sendModul();
}

//RS485使能
void SG805Robot::rs485_enable()
{
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = RS485_ENABLE;
#ifdef SUCCESS_INFO
    cout << "RS485_ENABLE\n";
#endif
    sendModul();

    rs485_flag = true;
}

//RS485失能
void SG805Robot::rs485_disable()
{
    send_len = 3;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = RS485_DISABLE;
#ifdef SUCCESS_INFO
    cout << "RS485_DISABLE\n";
#endif
    sendModul();

    rs485_flag = false;
}

//设置可动关节数量
void SG805Robot::joints_count()
{
    send_len = 4;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = JOINTS_COUNT;
    sendBuffer[2] = 6; //6个关节
    cout << "JOINTS_COUNT\n";
    sendModul();
}

//手动设置LOCATION，给当前机械臂位置赋值，用于机械臂位置校准,通过location_setting实例进行设置，然后再调用此函数
//但是必须在ros机械臂完全停止运动的时候进行，同时建议在调用此函数前进行
void SG805Robot::location_setting()
{
    send_len = 3 + LocationTCPDataLength;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = LOCATION_SETTING;
    memcpy(sendBuffer + 2, &location_setting_handle, LocationTCPDataLength);
    //cout << "LOCATION_SETTING\n";
    sendModul();
}

/*
 * 一体化伺服协议系统指令引入
 *  
*/

extern uint8_t disableServoCMD[6][8]; //引入编码器数据帧
extern uint8_t enableServoCMD[6][8];  //引入编码器数据帧

extern uint8_t saveParamerCMD[7][8]; //引入编码器数据帧
extern uint8_t setIdCMD[6][8];       //引入编码器数据帧

extern uint8_t resetTurnCMD[6][8];    //9位多圈编码器圈数清零
extern uint8_t versionRequestCMD[6][8];       //获取版本号
extern uint8_t positionRequestCMD[6][8]; //引入编码器数据帧

extern uint8_t setNumeratorCMD[6][8];   //设置电子齿轮比 分子
extern uint8_t setDenominatorCMD[6][8]; //设置电子齿轮比 分母

extern uint8_t distanceRequestCMD[6][8];    //获得距离传感器数据


/*
 * 一体化伺服协议系统指令引入
 *  
*/

void SG805Robot::getEncoders()
{
    if ((!usart_flag) || (!rs485_flag))
    {
        //关闭巡回资源
        upload_stop();

        //使能485
        rs485_enable();
        usleep(100000);

        //开启串口通信
        usart_start();
        usleep(100000);
    }

    //根据编码器工厂进行通信设计，如果编码器是被动方式，数据放入到缓存中
    usart_tx_len = 8;

    // memcpy(usartTXBuffer, positionRequestCMD[0], usart_tx_len);
    // usart_send();  //调用发送
    // usleep(50000); //50ms

    // memcpy(usartTXBuffer, positionRequestCMD[1], usart_tx_len);
    // usart_send();  //调用发送
    // usleep(50000); //50ms

    // memcpy(usartTXBuffer, positionRequestCMD[2], usart_tx_len);
    // usart_send();  //调用发送
    // usleep(50000); //50ms

    // memcpy(usartTXBuffer, positionRequestCMD[3], usart_tx_len);
    // usart_send();  //调用发送
    // usleep(50000); //50ms

    // memcpy(usartTXBuffer, positionRequestCMD[4], usart_tx_len);
    // usart_send();  //调用发送
    // usleep(50000); //50ms

    // memcpy(usartTXBuffer, positionRequestCMD[5], usart_tx_len);
    // usart_send();  //调用发送
    // usleep(50000); //50ms
    
    //cout << "GetEncoder\n";
    for (int i = 0; i < 6; i++)
    {
        memcpy(usartTXBuffer, positionRequestCMD[i], usart_tx_len);
        usart_send();  //调用发送
        usleep(50000); //50ms
    }

    // //关闭串口通信
    usart_stop();
    usleep(100000);

    // //关闭485
    rs485_disable();
    usleep(100000);

    //恢复巡回资源
    upload_start();
    usleep(100000);

    //#ifdef SUCCESS_INFO
    //输出数据
    // cout << "编码器数据为:";
    // for (int i = 0; i < 6; i++)
    // {
    //     cout << encoderAngle[i] << " ";
    // }
    // cout << endl;

    // for (int i = 0; i < 6; i++)
    // {
    //     cout << hex << encoderAngle[i] << " ";
    // }
    // cout << dec << endl
    //      << endl;
    //#endif
}

//通过对编码器的读取，获取机器人各个关节的角度
void SG805Robot::getPose()
{
    //等待获得编码器数据
    getEncoders();

    //计算脉冲数量
    for (int i = 0; i < 6; i++)
    {
        //存放获取到的位置信息
        location_setting_handle.position[i] = encoderPosition[i] * encoderPositiveOrNegative[i] / 16;
        cout << "location_setting_handle.position[i] " << location_setting_handle.position[i] << endl;
    }
}

void SG805Robot::return_to_zero()
{
    //     cout << "return_to_zero\n";

    //     //设置一个执行点
    //     NumberOfPoints = 1;

    //     //先进行清零
    //     memset(&trajectory[0], 0, PointSize);

    //     //获得各个关节的脉冲角度信息，并存放在location_setting_handle中
    //     getPose();

    //     //计算脉冲数量
    //     for (int i = 0; i < 6; i++)
    //     {
    //         trajectory[0].position[i] =  -location_setting_handle.position[i];
    //     }

    //     //无需运动的关节，将于10ms关闭
    //     trajectory[0].period[0] = 1000;
    //     trajectory[0].period[1] = 1000;
    //     trajectory[0].period[2] = 1000;
    //     trajectory[0].period[3] = 1000;
    //     trajectory[0].period[4] = 1000;
    //     trajectory[0].period[5] = 1000;

    //     //设置执行速度，慢点稳点,5s恢复
    //     trajectory[0].duration = 5000000;

    //     //调用sg805Robot中的sendTrajectory进行发送数据的操作

    //     //#ifdef SUCCESS_INFO
    //     //printTrajectory();
    //     //#endif

    //     //发送数据
    //     sendTrajectory();
}



void SG805Robot::disableServo(int id, int isAll)
{
    if ((!usart_flag) || (!rs485_flag))
    {
        //关闭巡回资源
        upload_stop();

        //使能485
        rs485_enable();
        usleep(100000);

        //开启串口通信
        usart_start();
        usleep(100000);
    }

    //根据编码器工厂进行通信设计，如果编码器是被动方式，数据放入到缓存中
    usart_tx_len = 8;

    if (isAll == 0)
    {
        memcpy(usartTXBuffer, disableServoCMD[id], usart_tx_len);
        usart_send();  //调用发送
        usleep(50000); //50ms
    }
    else if (isAll == 1)
    {
        for (int i = 0; i < 6; i++)
        {
            memcpy(usartTXBuffer, disableServoCMD[i], usart_tx_len);
            usart_send();  //调用发送
            usleep(50000); //50ms
        }
    }

    //关闭串口通信
    usart_stop();
    usleep(100000);

    // //关闭485
    rs485_disable();
    usleep(100000);

    //恢复巡回资源
    upload_start();
    usleep(100000);
}

void SG805Robot::enableServo(int id, int isAll)
{
    if ((!usart_flag) || (!rs485_flag))
    {
        //关闭巡回资源
        upload_stop();

        //使能485
        rs485_enable();
        usleep(100000);

        //开启串口通信
        usart_start();
        usleep(100000);
    }

    //根据编码器工厂进行通信设计，如果编码器是被动方式，数据放入到缓存中
    usart_tx_len = 8;
    if (isAll == 0)
    {
        memcpy(usartTXBuffer, disableServoCMD[id], usart_tx_len);
        usart_send();  //调用发送
        usleep(50000); //50ms
    }
    else if (isAll == 1)
    {
        for (int i = 0; i < 6; i++)
        {
            memcpy(usartTXBuffer, disableServoCMD[i], usart_tx_len);
            usart_send();  //调用发送
            usleep(50000); //50ms
        }
    }

    // //关闭串口通信
    usart_stop();
    usleep(100000);

    // //关闭485
    rs485_disable();
    usleep(100000);

    //恢复巡回资源
    upload_start();
    usleep(100000);
}
    
void SG805Robot::setServoID(int id)
{ //设置编码器ID编号并保存
    if ((!usart_flag) || (!rs485_flag))
    {
        //关闭巡回资源
        upload_stop();

        //使能485
        rs485_enable();
        usleep(100000);

        //开启串口通信
        usart_start();
        usleep(100000);
    }

    //根据编码器工厂进行通信设计，如果编码器是被动方式，数据放入到缓存中
    usart_tx_len = 8;
    memcpy(usartTXBuffer, setIdCMD[id], usart_tx_len);
    usart_send(); //调用发送
    sleep(5);     //至少2s

    //广播地址
    memcpy(usartTXBuffer, saveParamerCMD[6], usart_tx_len);
    usart_send(); //调用发送
    usleep(100000);    //100ms

    // //关闭串口通信
    usart_stop();
    usleep(100000);

    // //关闭485
    rs485_disable();
    usleep(100000);

    //恢复巡回资源
    upload_start();
    usleep(100000);

    cout << "按ctrl+c 退出程序，并断电。下次上电时，参数自动设置完成" << endl;
}

 //多圈清零
void SG805Robot::resetTurns(int id)
{   
    //多圈绝对值清零
    if ((!usart_flag) || (!rs485_flag))
    {
        //关闭巡回资源
        upload_stop();

        //使能485
        rs485_enable();
        usleep(100000);

        //开启串口通信
        usart_start();
        usleep(100000);
    }

    //根据编码器工厂进行通信设计，如果编码器是被动方式，数据放入到缓存中
    usart_tx_len = 8;
    memcpy(usartTXBuffer, resetTurnCMD[id], usart_tx_len);
    cout << "resetTurnCMD " << hex;
    for (int i = 0; i < 8; i++)
    {
        cout << (int8_t)resetTurnCMD[id][i] << " ";
    }
    cout << dec << endl;
    
    usart_send(); //调用发送
    sleep(5);       //至少等待2s

    //广播地址
    memcpy(usartTXBuffer, saveParamerCMD[6], usart_tx_len);
    usart_send(); //调用发送
    usleep(50000);    //50ms

    // //关闭串口通信
    usart_stop();
    usleep(100000);

    // //关闭485
    rs485_disable();
    usleep(100000);

    //恢复巡回资源
    upload_start();
    usleep(100000);
    cout << "按ctrl+c 退出程序，并断电。下次上电时，参数自动设置完成" << endl;
}


void SG805Robot::getServoVersion(int id)
{   
    //多圈绝对值清零
    if ((!usart_flag) || (!rs485_flag))
    {
        //关闭巡回资源
        upload_stop();

        //使能485
        rs485_enable();
        usleep(100000);

        //开启串口通信
        usart_start();
        usleep(100000);
    }

    //根据编码器工厂进行通信设计，如果编码器是被动方式，数据放入到缓存中
    usart_tx_len = 8;
    memcpy(usartTXBuffer, versionRequestCMD[id], usart_tx_len);
    cout << "versionRequestCMD " << hex;
    for (int i = 0; i < 8; i++)
    {
        cout << (int8_t)versionRequestCMD[id][i] << " ";
    }
    cout << dec << endl;
    
    usart_send(); //调用发送
    usleep(50000);  //50ms

    // //关闭串口通信
    usart_stop();
    usleep(100000);

    // //关闭485
    rs485_disable();
    usleep(100000);

    //恢复巡回资源
    upload_start();
    usleep(100000);
}


void SG805Robot::setServoSubdivision(int id)
{
    // 设置伺服的细分
    if ((!usart_flag) || (!rs485_flag))
    {
        //关闭巡回资源
        upload_stop();

        //使能485
        rs485_enable();
        usleep(100000);

        //开启串口通信
        usart_start();
        usleep(100000);
    }

    usart_tx_len = 8;

    cout << "分子设置中" << endl;
    memcpy(usartTXBuffer, setNumeratorCMD[id], usart_tx_len);
    usart_send(); //调用发送
    sleep(2);        //等待2s

    cout << "分母设置中" << endl;
    memcpy(usartTXBuffer, setDenominatorCMD[id], usart_tx_len);
    usart_send(); //调用发送
    sleep(2);        //等待2s

    cout << "保存设置中" << endl;
    memcpy(usartTXBuffer, saveParamerCMD[6], usart_tx_len);
    usart_send(); //调用发送
    usleep(100000);    //100ms


    // //关闭串口通信
    usart_stop();
    usleep(100000);

    // //关闭485
    rs485_disable();
    usleep(100000);

    //恢复巡回资源
    upload_start();
    usleep(100000);

    cout << "setServoSubdivision 电机细分设置完毕" << endl;

}


// 测距
int SG805Robot::getDistanceSensorData(int id)
{
    if ((!usart_flag) || (!rs485_flag))
    {
        //关闭巡回资源
        upload_stop();

        //使能485
        rs485_enable();
        usleep(100000);

        //开启串口通信
        usart_start();
        usleep(100000);
    }

    usart_tx_len = 8;
    memcpy(usartTXBuffer, distanceRequestCMD[id], usart_tx_len);
    cout << "distanceRequestCMD " << hex;
    for (int i = 0; i < 8; i++)
    {
        cout << (int8_t)distanceRequestCMD[id][i] << " ";
    }
    cout << dec << endl;
    
    usart_send(); //调用发送
    //usleep(50000);  //50ms

    bool flag = false;
    // 等待数据获取
    if (distanceSensorMutex.try_lock_for( * timeoutPtr_distance ))
    {
        std::cout << "测距仪数据 响应" << std::endl;
        flag = true;
        //int distanceData;
    }
    else
    {
        std::cout << "测距仪数据等待超时" << std::endl;
        distanceData = 0;
        flag = false;
    }

    // //关闭串口通信
    usart_stop();
    usleep(100000);

    // //关闭485
    rs485_disable();
    usleep(100000);

    //恢复巡回资源
    upload_start();
    usleep(100000);

    return distanceData;
}


// 单点模式移动
void SG805Robot::fastPointMove()
{
    //cout << "单点模式移动" << endl;
    send_len = 3 + FastPointSize;
    sendBuffer[0] = Sequence;
    sendBuffer[1] = FAST_POINT;
    memcpy(sendBuffer + 2, &fast_point_handle, FastPointSize);

    // cout << "fastPointMove 长度 " << send_len << endl;
    // cout << "数据 :" << hex << (int)sendBuffer[0] << " " << (int)sendBuffer[1];

    // for (int i = 2; i < send_len - 1; i++)
    // {
    //     if ((i-2) % 32 == 0)
    //     {
    //         cout << endl;
    //     }
    //     cout << (int)sendBuffer[i] << " ";
    // }
    // cout << dec << endl;
    sendModul();
}
