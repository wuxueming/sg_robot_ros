#ifndef SG805SERVO_H
#define SG805SERVO_H

#include "common.h"

//16.1  2022/04/30
//总线伺服

extern uint8_t crcHigh[256];
extern uint8_t crcLow[256];

//crc16_Modbus
//Checksum 校验和.参数模型是CRC-16/MODBUS 多项式是8005
//内部已执行高/低CRC字节的交换
uint16_t CRC16_MODBUS(uint8_t *_pBuf, int _usLen);
//校验编码器数据是否正确
bool checkEncoderData(uint8_t *_pBuf, int _usLen);




//检测编码器状态
int getEncoderAlarm(uint8_t *_pBuf);

//编码器ID
int getEncoderID(uint8_t *_pBuf);

//编码器位置数据
int getEncoderTurns(int position);
int getEncoderAngle(int position);
int getEncoderPosition(uint8_t *_pBuf);

// 距离获取
int getDistance(uint8_t *_pBuf);




#endif  //SG805SERVO_H
